import IMG from '@/utils/imageMange.js'

const hidePhone = function(value) {
	var tel = value
	console.log(typeof(tel))
	
	var reg = /^(\d{3})\d{4}(\d{4})$/;
	
	tel = tel.replace(reg, "$1****$2");
	return tel
}

const hideID = function(value) {
	var tel = value
	var reg = /^(\d{3})\d{11}(\w{4})$/;
	
	tel = tel.replace(reg, "$1****$2");
	return tel
}

const getPrice = function(price) {
	price = price || 0;
	return price.toFixed(2)
}

const empty = function(value) {
	let val = ''
	if(value) val = value
	else val = '暂未设置'
	return val
}

const zero = function(value) {
	let val = ''
	if(value) val = value
	else val = '0'
	return val
}

const defAvatar = function(value) {
	let val = ''
	if(value) val = value
	else val = '/yys/avatar.jpeg'
	return val
}

const orderStatus = function(value) {
	let val = ''
	console.log(value)
	switch(value){
		case 1:
			val = '待检测'
			break
		case 2:
			val = '检测中'
			break
		case 3:
			val = '已完成'
			break
		case 4:
			val = '已购买'
			break
		case 5:
			val = '待评价'
			break
		case 0:
			val = '未付款'
			break
		case '06':
			val = '售后中'
			break
		case '07':
			val = '售后中'
			break
		
		}
	return val
	
}

// 分类过滤
const markets = function(value){
	let val = ''
	switch(value){
		case '01':
			val = '全国市场'
			break
		case '02':
			val = '同城市场'
			break
		case '03':
			val = '共享仓'
			break
		default:
			val = '请选择市场分类'
	}
	return val
}

// 审核状态过滤
const authStatus = function(value){
	let val = ''
	switch(value){
		case '00':
			val = '审核中'
			break
		case '01':
			val = '未通过'
			break
		case '02':
			val = '已通过'
			break
	}
	return val
}


// 会员等级过滤
const userVip = function(value) {
	let val = ''
	console.log(value)
	switch(value){
		case '00':
			val = '普通会员'
			break
		case '01':
			val = '全国运营合伙人'
			break
		case '02':
			val = '同城运营合伙人'
			break
		case '03':
			val = '共享运营合伙人'
			break
	}
	return val
}

// 头像过滤
const avatar = function(value) {
	let val = ''
	if(!value) return
	if(value.includes('/static/imgs')) return value
	if(value.includes('http')) {
		val = value
	}else{
		val = IMG.img_url_global + value
	}
	
	if(value.includes('https://thirdwx.qlogo.cn/')){
		val = val
	}
	
	return val
}

const distribution = function(e){
	let a = ''
	switch(e){
		case '1':
			a = '配送'
			break
		case '2':
			a = '快运'
			break
		case '3':
			a = '包邮'
			break
		case '4':
			a = '到店服务'
			break
		case '5':
			a = '免配送费'
			break
	}
	
	return a
}

const video = function(e){
	let a = ''
	
	a = 'http://qn9mmfkvt.hb-bkt.clouddn.com' + e
	
	return a
	
	
}

const emaring = function(e){
	let a = ''
	switch(e){
		case '01':
			a = '配送收益'
			break
		case '02':
			a = '快运收益'
			break
		case '03':
			a = '配送收益'
			break
		case '04':
			a = '直推用户刷新信息收益'
			break
		case '05':
			a = '直推用户竞价排名收益'
			break
		case '06':
			a = '推广收益'
			break
		case '07':
			a = '市场分润'
			break
		case '08':
			a = '交易收入'
			break
	}
	
	return a
}

const timeToDate = function (val){
	if(!val) return
	let a = val.split(' ')[0]
	a = a.split('-')[1] + '—' + a.split('-')[2]
	return a
}

export default {
	hidePhone,
	hideID,
	getPrice,
	userVip,
	empty,
	markets,
	zero,
	defAvatar,
	orderStatus,
	authStatus,
	avatar,
	distribution,
	video,
	emaring,
	timeToDate
}