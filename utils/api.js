//正式接口文件
import http from './http.js'

//H5代理
//#ifdef  H5
//#endif  

//app无需代理
//#ifdef APP-PLUS
//#endif


export default {
	
	global:{
		// Banner
		getBanner(data){
			return http({
				yasuo:true,
				method:'get',
				url:'/slideshow',
				data:data
			})
		},
		
		getCode(data){
			return http({
				yasuo:true,
				method:'get',
				url:'/common/captcha',
				data:data
			})
		},
		
		getVersion(){
			return http({
				yasuo:true,
				method:'get',
				url:'/common/apps',
			})
		},
		
		getCategoryAll(){
			return http({
				yasuo:true,
				method:'get',
				url:'/core/datum-type/list',
			})
		},
		
		search(key){
			return http({
				yasuo:true,
				method:'get',
				url:'/core/datum/search?keyword=' + key ,
			})
		},
	},
	
	login:{
		pwdLogin(data){
			return http({
				yasuo:true,
				url:'/sysUser/sys-user/login',
				data:data
			})
		},
		
		regiset(data){
			return http({
				yasuo:true,
				url:'/sysUser/sys-user/register',
				data:data
			})
		},
		
		logout(data){
			return http({
				url:'/sysUser/sys-user/logout',
				data:data
			})
		},
	},
	
	teach: {
		getTeach(id, type){
			let a = null
			if(id == ''){
				 a = type
			} else {
				 a = id + '/' + type 
			}
			console.log(id, type)
			
			return http({
				method: 'get',
				url:'/core/datum/list/' + a,
			})
		},
	},
	
	user: {
		getUserInfo(data){
			return http({
				method:'get',
				url:'/sysUser/sys-user/userInfo',
				data:data
			})
		},
		
		// 
		uptUserinfo(data){
			return http({
				method:'put',
				url:'/sysUser/sys-user',
				data:data
			})
		},
		
		ruleOldPhone(data){
			return http({
				url:'/sysUser/sys-user/verifyPhone',
				data:data
			})
		},
		
		uptPhone(data){
			return http({
				method: 'put',
				url:'/sysUser/sys-user/updatePhone',
				data:data
			})
		},
		
		expertJoin(data){
			let method = 'post'
			if(data.method) method = data.method
			
			return http({
				method: method,
				url:'/sysUser/expert',
				data:data
			})
		},
		
		getUserExpert(data){
			return http({
				method: 'get',
				url:'/sysUser/expert',
				data:data
			})
		},
		
		getAddCategory(data){
			return http({
				method: 'get',
				url:'/core/datum-type/list',
				data:data
			})
		},
		
		addTeach(data){
			return http({
				url:'/core/datum',
				data:data
			})
		},
		
		getTeach(data){
			return http({
				method: 'get',
				url:'/core/datum/list/' + data.type,
			})
		},
		
		getSelfTeach(data){
			return http({
				method: 'get',
				url:'/core/datum/manage/' + data.type,
			})
		},
		
		delSelfTeach(data){
			return http({
				method: 'delete',
				url:'/core/datum/' + data.id,
			})
		},
		
		editExpert(data){
			return http({
				url:'/core/sys-user-expert-info',
				data: data
			})
		},
		
		expertList(data){
			let url = '/core/sys-user-expert-info/list'
			if(data.id) url = url + '/' + data.id
			
			return http({
				yasuo: true,
				method: 'get',
				url:url,
			})
		},
		
		getExpert(id){
			return http({
				method: 'get',
				url:'/core/sys-user-expert-info/' + id,
			})
		}
	},
	
	home: {
		
		addDiscernHistory(data){
			return http({
				url:'/core/crop-knowledge',
				data:data
			})
		},
		
		getDiscernHistory(data){
			return http({
				method: 'get',
				url:'/core/crop-knowledge/list',
				data:data
			})
		},
		
		getTeachDetail(data){
			return http({
				method: 'get',
				url:'/core/datum/' + data.id,
			})
		},
		
		addComment(data){
			return http({
				url:'/core/datum-comment',
				data: data
			})
		},
		
		zan(data){
			return http({
				url:'/core/datum-comment/like/' + data.id,
			})
		},
		
		unZan(data){
			return http({
				url:'/core/datum-comment/notlike/' + data.id,
			})
		},
		
		getBotanyName(data){
			return http({
				method: 'get',
				url:'/core/crop-type',
				data: data
			})
		},
		
		submitField(data){
			return http({
				url: '/core/field',
				data: data,
			})
		},
		
		getFieldList(data){
			return http({
				method: 'get',
				url: '/core/field/list',
				data: data,
			})
		},
		
		getFieldDetail(data){
			return http({
				method: 'get',
				url: '/core/field',
				data: data,
			})
		},
		
		getSelfCategory(data){
			return http({
				method: 'get',
				url: '/core/crop-type/myself',
				data: data,
			})
		},
		
		getFarming(data){
			return http({
				method: 'get',
				url: '/core/farming-type/list',
				data: data,
			})
		},
		
		addFarmimg(data){
			return http({
				url: '/core/farming',
				data: data,
			})
		},
		
		getHomeList1(){
			console.log()
			return http({
				method: 'get',
				yasuo:true,
				url: '/core/datum/list/01',
			})
		},
		
		getHomeList2(){
			console.log()
			return http({
				method: 'get',
				yasuo:true,
				url: '/core/datum/list/02',
			})
		},
		
		freeQA(data){
			return http({
				url: '/core/question',
				data: data,
			})
		},
		
		getFreeQAList(data){
			return http({
				method: 'get',
				url: '/core/question/list',
				data: data,
			})
		},
		
		getNoticeList(data){
			return http({
				method: 'get',
				url: '/core/official-notice/list',
				data: data,
			})
		},
		
		getNoticeDetail(data){
			return http({
				method: 'get',
				url: '/core/official-notice/' + data.id,
			})
		},
		
		getFieldHistory(data){
			
			return http({
				method: 'get',
				url: '/core/farming/' + data.id + '/' + data.year,
			})
		},
		
		
		addTakeLand(data){
			return http({
				url: '/core/soil-sample',
				data: data
			})
		},
	
		getTakeLandList(data){
			return http({
				method: 'get',
				url: '/core/soil-sample/list/' + data.status,
			})
		},
		
		getReportDetail(data){
			return http({
				method: 'get',
				url: '/core/soil-sample/' + data,
			})
		},
		
		buyGoods(data){
			return http({
				url: '/core/soil-sample/buy',
				data: data
			})
		},
		
		landTrace(data){
			return http({
				method: 'get',
				url: '/core/field/ascend/' + data.code,
			})
		},
		
	},
	
	store: {
		getGoodsList(data){
			return http({
				method: 'get',
				url: '/core/store/list',
			})
		},
	}
	
}
