function http(options) {
	// let defaultOptions = { method: 'post', url:'http://zhao.free.idcfengye.com', };
	let defaultOptions = { method: 'post', url:'http://192.168.2.169:8080', };
	// let defaultOptions = { method: 'post', url:'http://192.168.238.20:8081', };
	// let defaultOptions = { method: 'post', url:'http://152.136.145.188:8080', };
	let token = uni.getStorageSync('token')
	let overdue = uni.getStorageSync('overdue')
	
	defaultOptions.header = {
		"Content-Type": "application/json;charset=UTF-8",
		"Authorization":token
	}
	if(!options.pass){
		options.url = defaultOptions.url + options.url
	}
	
	options = Object.assign({}, defaultOptions, options);
	console.log('*********-------------//////////////')
	console.log(options)
	if(!options.yasuo){
		if(!token) {
			// 如果未登录 跳转登录页
			console.log(token, '我是token')
			return uni.navigateTo({
				url:'/pages/login/index?next=back'
			})
		}
	}
	return new Promise((resolve, reject)=>{
		options.success = res =>{
			console.log('************************')
			console.log(res)
			if(!res.data){
				uni.showToast({
					icon:'none',
					title:'系统异常'
				})
				return 
			}
			if(res.data.code == '200'){
				// console.log('************** 成功 **************** ')
				return resolve(res.data)
			} else {
				// if(!curRoute.includes('login/login')){
					
				// }
				
				if(res.data.code == 'A0210'){
					return reject(res.data)
				}
				let routes = getCurrentPages(); // 获取当前打开过的页面路由数组
				let curRoute = routes[routes.length - 1].route 
				if(curRoute == 'pages/login/login') return reject()
				if(res.data.code == '401'){
					return uni.navigateTo({
						url:'/pages/login/index?next=back'
					})
				}
				if(res.data.code == '401'){
					return uni.navigateTo({
						url:'/pages/login/index?next=back'
					})
				}
				if(!res.msg) res.msg = '系统异常'  
				console.log(res.data.msg)
				uni.showToast({
					icon:'none',
					title: res.data.msg
				})
				return reject(res.data)
			}
		}
		options.fail = rej =>{
			console.log(rej, '失败')
			return reject(rej.data)
		}
		uni.request(options)
	})
}


export default http;