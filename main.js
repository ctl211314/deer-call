import Vue from 'vue'
import App from './App'
import API from '@/utils/api.js'
import store from './store'
import IMG from '@/utils/imageMange.js'
import BUGNULL from '@/utils/nobug.js'
import filters from '@/utils/filter.js'
import Tian from '@/utils/tian.js'
import HTTP from '@/utils/http.js'
import uView from "uview-ui";
import {router,RouterMount} from './router.js'  //路径换成自己的
Vue.use(router)
Vue.use(uView);

Vue.config.productionTip = false

Vue.prototype.BUGNULL = BUGNULL
Vue.prototype.API = API
Vue.prototype.IMG = IMG
Vue.prototype.$Tian = Tian
Vue.prototype.$HTTP = HTTP
Vue.prototype.$store = store

App.mpType = 'app'

const app = new Vue({
    ...App,
	store
})

//过滤器统一处理加载
Object.keys(filters).forEach(key => {  
  Vue.filter(key, filters[key])  
})

//v1.3.5起 H5端 你应该去除原有的app.$mount();使用路由自带的渲染方式
// #ifdef H5
	RouterMount(app,router,'#app')
// #endif

// #ifndef H5
	app.$mount(); //为了兼容小程序及app端必须这样写才有效果
// #endif
