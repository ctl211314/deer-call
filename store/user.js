import API from '@/utils/api.js'

export default {
        namespaced: true,
        state: {
			userInfo:{},
			expert:{},
        },
        mutations: {
			_init() { 
				console.log('找到了')
			},
			
			// 修改当前使用的收货地址
			setUserInfo(state, payload){ state.userInfo = payload },
			
			setExpert(state, payload){ state.expert = payload },
        },
        actions: {
			fun1({ commit, dispatch }, payload){
				return API.addressManage.getAddress(payload)
					.then(res=>{
						commit('setAddrList', res.deliveryAddresses)
					})
			},
			
			
			getUserInfo({ commit, dispatch }, payload){
				let _token = uni.getStorageSync('token')
				if(!_token) return
				return API.user.getUserInfo(payload)
					.then(res=>{
						commit('setUserInfo', res.user)
						uni.setStorageSync('userInfo', res.user)
					})
			},
			
			uptUserinfo({ commit, dispatch }, payload){
				return new Promise((resolve, reject)=>{
					return API.user.uptUserinfo(payload)
						.then(res=>{
							dispatch('getUserInfo')
							resolve('success')
						})
				})
			},
			
			getUserExpert({ commit, dispatch }, payload){
				let _token = uni.getStorageSync('token')
				if(!_token) return
				return API.user.getUserExpert(payload)
					.then(res=>{
						commit('setExpert', res.data)
					})
			},
        }
		
};