import API from '@/utils/api.js'

export default {
        namespaced: true,
        state: {
			aaalist: [
				'别再让我东张西望'
			],
			addrList:[],
			defaultAddr:{},
        },
        mutations: {
			_init() { 
				console.log('找到了')
			},
			
			// 设置收货地址
			setAddrList(state, payload){ 
				state.addrList = payload 
				let data = payload
				for(let i in data){
					if(data[i].isDefault == '1') state.defaultAddr = data[i]
				}
			},
			
			// 修改当前使用的收货地址
			setDefaultAddr(state, payload){
				state.defaultAddr = payload
			}
        },
        actions: {
			init({ commit, dispatch }) {
				console.log('找到没')
			},
			
			getAddrList({ commit, dispatch }, payload){
				return API.addressManage.getAddress(payload)
					.then(res=>{
						commit('setAddrList', res.deliveryAddresses)
					})
			},
			
        }
		
};